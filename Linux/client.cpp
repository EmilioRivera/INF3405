#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <iostream>
#define bzero(b,len) (memset((b), '\0', (len)), (void) 0)  
#define bcopy(b1,b2,len) (memmove((b2), (b1), (len)), (void) 0)
void error(const char *msg)
{
    perror(msg);
    exit(0);
}

#define CLI_ARGS 0

int main(int argc, char *argv[])
{
    int sockfd, portno, n;
    int choiceMade = 0;
    struct sockaddr_in serv_addr;
    struct hostent *server;
    
    char buffer[256];
    char mess[256];
    char wait;
	std::string adConn;
	int portConn;
#if (CLI_ARGS==1)
    if (argc < 3) {
       fprintf(stderr,"usage %s hostname port\n", argv[0]);
       exit(0);
    }
    portno = atoi(argv[2]);
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) 
        error("ERROR opening socket");
    server = gethostbyname(argv[1]);
    if (server == NULL) {
        fprintf(stderr,"ERROR, no such host\n");
        exit(0);
    }
#else
	printf("Veuillez entrer le nom d'hôte du serveur de vote \n");
	std::cin >> adConn;
	
	printf("Veuillez entre le port sur lequel se connecter (entre 5000 et 5050) \n");
	std:: cin >> portConn;
	
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd < 0)
		error("ERROR opening socket");
	portno = portConn;
	server = gethostbyname(adConn.c_str());
	if (server == NULL)
	{
		fprintf(stderr, "ERROR, no such host \n");
		exit(0);
	}
	
#endif
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr, 
         (char *)&serv_addr.sin_addr.s_addr,
         server->h_length);
    serv_addr.sin_port = htons(portno);
    if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) 
        error("ERROR connecting");
    
    bool exitReceived = false;


    ///////////////////////////////////////////////////////////////////
    // We have a successful connection let's do what the application //
    // is supposed to actually do 				     //
    ///////////////////////////////////////////////////////////////////


    
    // On acquiert le nombre de candidats possibles
    unsigned int nbCandidats;
    // n = read(sockfd, &nbCandidats, sizeof(unsigned int));
    n = recv(sockfd, &nbCandidats, sizeof(unsigned int), MSG_WAITALL );

    
    printf("Nombre de candidats : %d\n", nbCandidats);
    bzero(buffer,256);    
    for (unsigned int i = 1 ; i <= nbCandidats; i++)
    {
	read(sockfd, buffer, sizeof(buffer)); 
	printf("Candidat %d : %s\n", i, buffer);
	bzero(buffer,256); 
    }
    
    // All candidates have been displayed
    // Ask for choice of the user
    std::cout << "Veuillez choisir le numéro du candidat que vous voulez élire : ";
    std::cin >> choiceMade;
    std::cout << "Mon choix : " << choiceMade << std::endl;
    n = write(sockfd, &choiceMade, sizeof(choiceMade));
    
    close(sockfd);
    return 0;
}
