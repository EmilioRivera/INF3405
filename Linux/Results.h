#ifndef _RESULTS_H_
#define _RESULTS_H_
#include <vector>
#include <unordered_map>
#include <string>
#include <vector>

// Perhaps this should be a singleton

using namespace std;

class Results
{
 public:
  Results(const vector<string>& theCandidates);
  
  bool incrementByValue(const unsigned int choice);
  bool incrementByName (const string& candidateName);
  
  ~Results();

 private:
  unsigned int numberOfCandidates_;
  unordered_map <string,unsigned int> resultByName_;
  vector<unsigned int> resultByValue_;
  
};







#endif // _RESULTS_H_
