#include <stdio.h>
#include <string.h>   //strlen
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>   //close
#include <arpa/inet.h>    //close
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/time.h> //FD_SET, FD_ISSET, FD_ZERO macros
#include <pthread.h>  
#include <iostream>
#include <fstream>
#include <vector>
#include <time.h>
#define bzero(b,len) (memset((b), '\0', (len)), (void) 0)  
#define bcopy(b1,b2,len) (memmove((b2), (b1), (len)), (void) 0)

#define TRUE   1
#define FALSE  0
#define PORT 8888

#define TP_NB_PORTS 51
#define TP_BASE_ADDRESS 5000

using namespace std;

/* Prototype functions*/

void* myFunction(void* pData);
void lireCandidats(vector<string>& candidats,const string& fileName); 
void communication(int sock);
const std::string currentDateTime();

struct tpData
{
    int portNumber_;
    int choiceMade_;    
};

struct thread_info {    /* Used as argument to thread_start() */
    pthread_t thread_id;        /* ID returned by pthread_create() */
    int       thread_num;       /* Application-defined thread # */
    struct tpData* data;      /* From command-line argument */
};


    
int main (void)
{
    struct thread_info* threads;
    int retValues[TP_NB_PORTS] = {0};
    
    threads = new thread_info[TP_NB_PORTS];
     
    for (int i = 0; i <  TP_NB_PORTS; i++)
    {
		threads[i].data = new tpData;
		
		threads[i].thread_num = 0 + i;
		threads[i].data->portNumber_ = TP_BASE_ADDRESS + i;
		retValues[i] = pthread_create(&threads[i].thread_id,
						  NULL,
						  myFunction,
						  &threads[i]);
		if (retValues[i] != 0)
		{
			printf("Error while creating the thread %d\n", i);
			
		}
    }
    
    // Eventually join all
    while(1) {
    }
    
    // Free the memory
    for (int i = 0; i < TP_NB_PORTS; i++)
	delete threads[i].data;
    delete [] threads;
}

void* myFunction(void* pData)
{   
	string tConn;
	while (1)
	{
		struct thread_info* theInfo = (thread_info*) pData;
		printf("Here is thread %d with port number : %d \n", theInfo->thread_num, theInfo->data->portNumber_);
		/* Code copié de l'autre TP*/
		int mySocket;
		int theNewSocket;
		socklen_t clilen;
		struct sockaddr_in serv_addr, cli_addr;

		char wait;

		mySocket = socket(AF_INET, SOCK_STREAM, 0);
		if (mySocket < 0) 
		printf("Error while opening socket \n");
		
		bzero((char *) &serv_addr, sizeof(serv_addr));
		
		serv_addr.sin_family = AF_INET;
		serv_addr.sin_addr.s_addr = INADDR_ANY;
		serv_addr.sin_port = htons(theInfo->data->portNumber_); 
		
		if (::bind(	mySocket,
					(struct sockaddr *) &serv_addr,
					(socklen_t) sizeof(serv_addr)
					)
			< 0) 
		printf("Error on binding \n");
		listen(mySocket,5);
		clilen = sizeof(cli_addr);
		
		// printf("[%d] Waiting for a connection ... \n", theInfo->thread_num);
		
		theNewSocket = accept(mySocket, 
				  (struct sockaddr *) &cli_addr,
				  &clilen);
		if (theNewSocket < 0) 
			printf("Error on ACCEPT \n");
		else // A connection was successfully made
			while(1)
			{
				tConn = currentDateTime();
				printf("[%s] Connection made with IP %s on port %d\n", tConn.c_str(), inet_ntoa(cli_addr.sin_addr), cli_addr.sin_port);
				printf("[%d]Waiting for information \n", theInfo->thread_num);
				communication(theNewSocket);
				close(theNewSocket);
				theNewSocket = accept(mySocket,
										(struct sockaddr *) &cli_addr,
										&clilen);
			}
		//close(mySocket);
	}
    return 0;

}

void lireCandidats(vector<string>& candidats,const string& fileName)
{
	ifstream fichier;
	string theLine = "";
	fichier.open(fileName.c_str(), iostream::in); 
	if (fichier.fail())
		cout << "Erreur dans l'ouverture du fichier" << endl;
	else
	{
		while( getline(fichier, theLine) )
		{ 
		  candidats.push_back(theLine);
		  //cout << "Lu la ligne " << theLine << endl;
		}
		fichier.close();
    }
}

void communication (int sock)
{
	bool exitReceived = false;
	unsigned int userChoice;
	int n;
	int count = 0;
	char mess[256];
	char buffer[256];
	string tmp;
	vector<string> lesCandidats;

	// Read available candidates from file
	lireCandidats(lesCandidats, "Candidats.txt"); 
	unsigned int nbCandidats = lesCandidats.size();

	// Transmit the number of candidates
	n = write(sock, &nbCandidats, sizeof(unsigned int));

	for (vector<string>::const_iterator i = lesCandidats.begin(); i != lesCandidats.end() ; i++)
	{    
		// For actually sending all candidates
		bzero(mess,256);
		tmp = *i;
		//cout <<"Sending candidate " << *i << endl;
		strncpy(mess, tmp.c_str(), sizeof(mess));
		write(sock, mess, sizeof(mess));
	}
	// bzero(buffer,256); // Only use for strings or non-standard types
	n = read (sock, &userChoice, sizeof(userChoice));
	cout << "Choix fait par l'utilisateur " << userChoice << " : "
	<< lesCandidats[userChoice - 1] << endl;      
	/* Client terminated connection or count exceeded*/ 
	printf("Out of the loop / terminated \n");
}

//;http://stackoverflow.com/questions/997946/how-to-get-current-time-and-date-in-c
// Get current date/time, format is YYYY-MM-DD.HH:mm:ss
const std::string currentDateTime() {
	time_t     now = time(0);
    struct tm  tstruct;
    char       buf[80];
    tstruct = *localtime(&now);
    // Visit http://en.cppreference.com/w/cpp/chrono/c/strftime
    // for more information about date/time format
    strftime(buf, sizeof(buf), "%Y-%m-%d.%X", &tstruct);

    return buf;
}


