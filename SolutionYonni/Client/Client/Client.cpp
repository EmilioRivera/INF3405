#undef UNICODE

#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include <iostream>
#include <string>
#include <algorithm>
#include <cctype>
#include <time.h>

#define bzero(b,len) (memset((b), '\0', (len)), (void) 0)  
#define bcopy(b1,b2,len) (memmove((b2), (b1), (len)), (void) 0)
#define VOTE_ALEATOIRE 1

// Link avec ws2_32.lib
#pragma comment(lib, "ws2_32.lib")

// Traitement du vote
std::string TraiterVote(const std::vector<std::string>& liste);
std::string VoteAleatoire(const std::vector<std::string>& candidates);
bool EstNumerique(const std::string& s);

int __cdecl main(int argc, char **argv)
{
	WSADATA wsaData;
	SOCKET leSocket;
	struct addrinfo *result = NULL, *ptr = NULL, hints;

	char motRecu[1024];
	int iResult;
	std::vector<std::string> candidates;

	// InitialisATION de Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) 
	{
		printf("Erreur de WSAStartup: %d\n", iResult);
		return 1;
	}

	// On va creer le socket pour communiquer avec le serveur
	leSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (leSocket == INVALID_SOCKET)
	{
		printf("Erreur de socket(): %ld\n\n", WSAGetLastError());
		freeaddrinfo(result);
		WSACleanup();
		printf("Appuyez une touche pour finir\n");
		getchar();
		return 1;
	}

	// On va chercher l'adresse du serveur en utilisant la fonction getaddrinfo.
	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;        // Famille d'adresses
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;  // Protocole utilis� par le serveur

	// On indique le nom et le port du serveur auquel on veut se connecter
	std::string ipAddr;
	std::string portNum;
	
	// Lecture du clavier de l'addresse IP
	std::cout << "Entrez l'addresse IP : ";
	std::cin >> ipAddr;
	char *host = (char*)ipAddr.c_str();
	
	// Lecture du clavier du port a se connecter. Accepte que les valeurs 5000 a 5050
	int portT = 0;
	do
	{
		std::cout << "Entrez le port voulu ( entre 5000 et 5050 ) : ";		
		std::cin >> portNum;		
		portT = atoi(portNum.c_str());
	} while (portT < 5000 || portT > 5050);

	std::cin.clear();
	std::cin.ignore();

	char *port = (char*)portNum.c_str();

	// getaddrinfo obtient l'adresse IP du host donn�
	iResult = getaddrinfo(host, port, &hints, &result);
	if (iResult != 0) 
	{
		printf("Erreur de getaddrinfo: %d\n", iResult);
		WSACleanup();
		return 1;
	}
	std::cin.clear();

	//On parcours les adresses retournees jusqu'a trouver la premiere adresse IPV4
	while ((result != NULL) && (result->ai_family != AF_INET))
		result = result->ai_next;

	//-----------------------------------------
	if (((result == NULL) || (result->ai_family != AF_INET)))
	{
		freeaddrinfo(result);
		printf("Impossible de recuperer la bonne adresse\n\n");
		WSACleanup();
		printf("Appuyez une touche pour finir\n");
		getchar();
		return 1;
	}

	sockaddr_in *adresse;
	adresse = (struct sockaddr_in *) result->ai_addr;
	//----------------------------------------------------
	std::cout << std::endl;
	printf("Adresse trouvee pour le serveur %s : %s\n", host, inet_ntoa(adresse->sin_addr));
	printf("Tentative de connexion au serveur %s avec le port %s\n", inet_ntoa(adresse->sin_addr), port);

	// On va se connecter au serveur en utilisant l'adresse qui se trouve dans la variable result.
	iResult = connect(leSocket, result->ai_addr, (int)(result->ai_addrlen));
	if (iResult == SOCKET_ERROR)
	{
		printf("Impossible de se connecter au serveur %s sur le port %s\n\n", inet_ntoa(adresse->sin_addr), port);
		freeaddrinfo(result);
		WSACleanup();
		printf("Appuyez une touche pour finir\n");
		getchar();
		return 1;
	}

	printf("Connecte au serveur %s:%s\n\n", host, port);
	freeaddrinfo(result);

	// Maintenant, on va recevoir l'information envoy�e par le serveur
	bzero(motRecu, 1024);
	iResult = recv(leSocket, motRecu, 1024, 0);
	if (iResult > 0)
	{
		std::cout << "Voici la liste des candidats :" << std::endl;

		// Parse la string en enlevant les ";"
		std::string s = motRecu;
		std::string delimiter = ";";

		size_t pos = 0;
		std::string token;
		int i = 1;
		while ((pos = s.find(delimiter)) != std::string::npos)
		{
			token = s.substr(0, pos);
			std::cout << std::to_string(i) + ". " + token + ";" << std::endl;
			candidates.push_back(token);
			s.erase(0, pos + delimiter.length());
			i++;
		}
		std::cout << std::to_string(i) + ". " + s << std::endl << std::endl;
		candidates.push_back(s);
	}
	else
	{
		printf("Erreur de reception : %d\n", WSAGetLastError());
	}

	std::string message;
	//////////////////////////////////////////////////
	//				Traitement du vote				//

	if (VOTE_ALEATOIRE)
	{
		message = VoteAleatoire(candidates);
	}
	else
	{
		message = TraiterVote(candidates);
	}
	//												//
	//////////////////////////////////////////////////

	// Envoyer le mot au serveur
	iResult = send(leSocket, message.c_str(), message.size(), 0);
	if (iResult == SOCKET_ERROR)
	{
		if (WSAGetLastError() == 10054)
		{
			std::cout << "Le serveur a ferme la connection. Vous avez ete trop lent.\n";
		}
		else
		{
			printf("Erreur du send: %d\n", WSAGetLastError());
		}
		closesocket(leSocket);
		WSACleanup();
		printf("Appuyez une touche pour finir\n");
		getchar();
		return 1;
	}

	// cleanup
	closesocket(leSocket);
	WSACleanup();

	printf("Appuyez une touche pour finir\n");
	getchar();
	return 0;
}


/**
	Fonction qui fait un vote aleatoire sur l'index d'un vecteur de strings
	\param candidates  vector<std::string> contenant les candidats
	\return Le nom du candidat choisi par la fonction
	Note: La fonction demande a l'usager de faire des entres tant de fois qu'il
	ne soumet pas une entree valide. l'orthographe ainsi que les lettres
	majuscules comptent.

*/
std::string TraiterVote(const std::vector<std::string>& candidates)
{
	std::string message = "";
	std::string messageFinal = "";
	bool isCorrect = false;

	// Demander � l'usager un mot a envoyer au serveur
	do
	{
		printf("Veuillez faire votre choix.\n Vous pouvez entrer un nom ou un numero de candidat. : ");
		std::getline(std::cin, message);
		
		// Traiter les nombres
		if (EstNumerique(message))
		{
			int index = atoi(message.c_str());
			if (index <= candidates.size() && index > 0)
			{
				isCorrect = true;
				messageFinal = candidates[index - 1];
			}
		}
		else
		{
			for each (std::string s in candidates)
			{
				if (!strcmp(s.c_str(), message.c_str()))
				{
					isCorrect = true;
					messageFinal = message;
				}
			}
		}
	} while (!isCorrect);
	std::cin.clear();
	return messageFinal;
}

/**
	Fonction qui fait un vote aleatoire sur l'index d'un vecteur de strings
	\param candidates  vector<std::string> contenant les candidats
	\return Le nom du candidat choisi par la fonction
	Note: etant donne l'utilisation de rand() et %, le choix est un biaise
	pour des tres grandes listes : rand() retourne un nombre de 32bits.

*/
std::string VoteAleatoire(const std::vector<std::string>& candidates)
{
	std::string candidatChoisi = "";
	int randVote = 0;
	srand(time(NULL));
	randVote = rand() % candidates.size();
	candidatChoisi = candidates[randVote];
	return candidatChoisi;
}

/**
	Fonction qui fait test si un string reperesente un nombre Entier
	\param s  std::string <-- le string a verifier
	\return si la chaine en entree n'est pas vite et ne contient que des nombres
	Note: etant donne qu'on teste que pour des nombres. La fonction ne retourne 
	vrai que dans le cas d'un nombre entier positif.

*/
bool EstNumerique(const std::string& s)
{
	std::string::const_iterator it = s.begin();
	while (it != s.end() && std::isdigit(*it)) ++it;
	return !s.empty() && it == s.end();
}