#include <Windows.h>
#include <iostream>
#include <fstream>
#include <ctime>
#include <chrono>
#include <map>
#include <ratio>
#include <string>
#define bzero(b,len) (memset((b), '\0', (len)), (void) 0)  
#define bcopy(b1,b2,len) (memmove((b2), (b1), (len)), (void) 0)

#pragma comment( lib, "ws2_32.lib" )
#pragma warning(disable:4996)
#define MAX_THREADS 50
#define STARTING_PORT 5000

// IP address a changer pour le IP courant de la machine
#define IP_ADDRESS "132.207.223.250"

// Fonctions externes
DWORD WINAPI EchoHandler(void* sd_);
DWORD WINAPI ClockHandler(void* sd_);
DWORD WINAPI TraiterPort(LPVOID param);
void LireFichier();
void AfficherResultat();


// Structure de l'information des SOCKETS/ports
typedef struct Data
{
	SOCKET socket;
	int port;

} Data, *PDATA;

using namespace std;

// Tableau des r�sultats
map<string, int> resultat;

// Fichier Journal des electeurs 
std::ofstream fout("log.txt");

// Mutex pour la gestion concurrente des ressources
HANDLE Mutex;

// Timer
std::time_t Debut;
chrono::duration < int, std::ratio< 5  >  > delay (1);
chrono::system_clock::time_point today = chrono::system_clock::now();
time_t tt = chrono::system_clock::to_time_t(today);
tm local_tm = *localtime(&tt);
int endMinute = 0;
int endHour = 0;
bool closeSockets = true;

int main()
{
	// Initialiser WinSock
	WSADATA WsaDat;
	if (WSAStartup(MAKEWORD(2, 2), &WsaDat) != 0)
	{
		std::cout << "WSA Initialization failed!\r\n";
		WSACleanup();
		system("PAUSE");
		return 0;
	}

	LireFichier();
	fout << "================== Debut du log ==========================\n";
	fout << "Le serveur ouvre a : " << ctime(&tt);
	//AfficherResultat();
	// Input des heures de fermeture du scrutin


	do
	{
		std::cout << "Entrez l'heure de fin du scrutin ( heure seulement) :";
		std::cin >> endHour;
		std::cout << "Entrez la minute de fun du scruit ( minutes seulement) :";
		std::cin >> endMinute;
		chrono::system_clock::time_point today = chrono::system_clock::now();
		time_t tt = chrono::system_clock::to_time_t(today);
		tm local_tm = *localtime(&tt);
	} while ((local_tm.tm_min / 60.0 + local_tm.tm_hour) > (endMinute / 60.0 + endHour));
	
	fout << "Le serveur ferme a : " << endHour << " : " << endMinute << ":00" <<std::endl << std::endl;
	std::cout << "Serveur demare a : " << ctime(&tt);
	std::cout << "Serveur ferme a : " << endHour << " : " <<  endMinute <<":00" << endl;
	
	DWORD nThreadID;
	CreateThread(NULL, 0, ClockHandler, 0, 0, &nThreadID);

	// Cr�er 50 threads. Chacun des threads s'occupe de g�rer
	// un des 50 ports disponibles. Le treads principal attends
	// que tous ses threads finissent avant de terminer.

	PDATA DataArray[MAX_THREADS];
	DWORD   dwThreadIdArray[MAX_THREADS];
	HANDLE  hThreadArray[MAX_THREADS];

	// Cr�er le mutex pour les acc�s concurrents � la structure de donn�es
	Mutex = CreateMutex(
						NULL,              // default security attributes
						FALSE,             // initially not owned
						NULL);             // unnamed mutex

	if (Mutex == NULL)
	{
		printf("CreateMutex error: %d\n", GetLastError());
		return 1;
	}

	for (int i = 0; i < MAX_THREADS; i++)
	{
		// Allocate memory for thread data.
		DataArray[i] = (PDATA)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, sizeof(Data));

		if (DataArray[i] == NULL)
		{
			// If the array allocation fails, the system is out of memory
			// so there is no point in trying to print an error message.
			// Just terminate execution.
			ExitProcess(2);
		}

		DataArray[i]->port = STARTING_PORT + i;

		// Create the thread to begin execution on its own.
		hThreadArray[i] = CreateThread(
										NULL,                   // default security attributes
										0,                      // use default stack size  
										TraiterPort,			// thread function name
										DataArray[i],			// argument to thread function 
										0,                      // use default creation flags 
										&dwThreadIdArray[i]);   // returns the thread identifier 

	}

	// Wait until all threads have terminated.
	WaitForMultipleObjects(MAX_THREADS, hThreadArray, TRUE, INFINITE);

	// D�sallouer la m�moire et d�truire les threads
	for (int i = 0; i < MAX_THREADS; i++)
	{
		CloseHandle(hThreadArray[i]);
		if (DataArray[i] != NULL)
		{
			HeapFree(GetProcessHeap(), 0, DataArray[i]);
			DataArray[i] = NULL;    // Ensure address is not reused.
		}
	}

	// D�truire le mutex;
	CloseHandle(Mutex);

	// Afficher les r�sultats des �lections
	std::cout << "RESULTATS FINAUX : \n";
	AfficherResultat();

	// Fermeture du ficher LOG 
	fout << "================== Fin du log ==========================\n";
	fout.close();
	system("pause");
	return 0;
}

/**
	Fonction qui fait la gestion de la connection sur des ports.
	\param param l'information du port sur lequel une connection est faite.
	\return
	Note: 
*/
DWORD WINAPI TraiterPort(LPVOID param)
{
	PDATA data = (PDATA)param;

	data->socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (data->socket == INVALID_SOCKET) 
	{	
		WSACleanup();
		return 1;
	}
	char* option = "1";
	setsockopt(data->socket, SOL_SOCKET, SO_REUSEADDR, option, sizeof(option));

	//Recuperation de l'adresse locale
	hostent *thisHost;
	
	thisHost = gethostbyname(IP_ADDRESS);
	char* ip;
	ip = inet_ntoa(*(struct in_addr*) *thisHost->h_addr_list);

	sockaddr_in service;
	service.sin_family = AF_INET;
	service.sin_addr.s_addr = inet_addr(ip);
	service.sin_port = htons(data->port);

	if (bind(data->socket, (SOCKADDR*)&service, sizeof(service)) == SOCKET_ERROR) 
	{
		closesocket(data->socket);
		WSACleanup();
		return 1;
	}

	// Listen for incoming connection requests on the created socket
	if (listen(data->socket, 10) == SOCKET_ERROR) 
	{
		closesocket(data->socket);
		WSACleanup();
		return 1;
	}

	printf("En attente des connections des clients sur le port %d...\n", ntohs(service.sin_port));

	while (closeSockets) 
	{
		sockaddr_in sinRemote;
		int nAddrSize = sizeof(sinRemote);

		// Create a SOCKET for accepting incoming requests.
		// Accept the connection.
		SOCKET sd = accept(data->socket, (sockaddr*)&sinRemote, &nAddrSize);
		if (sd != INVALID_SOCKET) 
		{
			std::cout << "Connection acceptee De : " <<
				inet_ntoa(sinRemote.sin_addr) << ":" <<
				ntohs(sinRemote.sin_port) << "." <<
				std::endl;
			fout << "**************************\n";

			DWORD nThreadID;
			CreateThread(0, 0, EchoHandler, (void*)sd, 0, &nThreadID);
		}
		else 
		{
			if (closeSockets)
			{
				std::cout << "Echec de connection" << std::endl;
			}
		}
	}

	// No longer need server socket
	closesocket(data->socket);

	WSACleanup();
	return 0;
}

/**
	Fonction qui fait la gestion de la communication.
	\param sd_ le socket sur lequel on fait la connection
	\return
	Note: lorsqu'on re�oit le vote, on met un mutex sur la MAP contenant
		  les candidats pour s'assurer qu'il n'y a pas de competition entre
		  les votes.
		  La fonction g�re aussi le journal des �lecteurs.
*/
DWORD WINAPI EchoHandler(void* sd_)
{
	SOCKET sd = (SOCKET)sd_;
	string message = "";
	int iResult;
	char motRecu[1024];

	struct sockaddr_in sin;
	int len = sizeof(sin);

	// Pr�parer le message � envoyer au client
	auto iter = resultat.begin();
	while (iter != resultat.end())
	{
		message += iter->first;
		iter++;
		message += ";";
	}

	message = message.substr(0, message.size() - 1);

	// Envoyer le message
	send(sd, message.c_str(), message.size(), 0);

	// Attendre la r�ponse du client
	bzero(motRecu, 1024);
	iResult = recv(sd, motRecu, 1024, 0);

	string reception = motRecu;
	auto it = resultat.begin();
	time_t _tm = time(NULL);

	struct tm * curtime = localtime(&_tm);
	getsockname(sd, (struct sockaddr *)&sin, &len);
	
	if (iResult > 0)
	{
		// V�rouiller la map et rafraichir les r�sultats
		DWORD waitResult;
		waitResult = WaitForSingleObject(Mutex, INFINITE);
		switch (waitResult)
		{
			case WAIT_OBJECT_0:
				it = resultat.find(reception);
				if (it != resultat.end())
				{
					resultat[reception]++;
					cout << "Vote recu le : " << asctime(curtime);
					cout << "Addresse IP :" << inet_ntoa(sin.sin_addr) << std::endl;
					cout << "Port :" << ntohs(sin.sin_port) << std::endl;
					fout << "Vote recu le : " << asctime(curtime);
					fout << "Addresse IP :" << inet_ntoa(sin.sin_addr) << std::endl;
					fout << "Port :" << ntohs(sin.sin_port) << std::endl;
				}
				else
				{
					std::cout << "\nL'electeur a donne un mauvais choix!!!\n";
				}
				ReleaseMutex(Mutex);
			break;

			case WAIT_ABANDONED:
				return 0;
			break;
		}
	}
	else
	{
		if (closeSockets)
			printf("Erreur de reception : %d\n", WSAGetLastError());
	}

	closesocket(sd);
	return 0;
}


/**
	Fonction qui fait un affiche les r�sultats du vote
	\param sd_  pour cette fonction, c'est un param�tre vide
	\return
	Note: la minuterie compare le temps pr�sent avec le temps specifi� par l'utilisateur.
	      Lorsqu'on arrive au temps d�sir�, on envoie un signal et on arr�te tous les autres
		  threads, fermant effectivement les connections et finissant le vote.

*/
DWORD WINAPI ClockHandler(void* sd_){
	
	bool counting = true;
	while (counting)
	{
		today = chrono::system_clock::now();
		tt = chrono::system_clock::to_time_t(today);
		tm local_tm = *localtime(&tt);
		if (local_tm.tm_min >= endMinute && local_tm.tm_hour >= endHour)
		{
			std::cout << " Delai expire. Le serveur va fermer!\n";
			std::cout << ctime(&tt);
			counting = false;
			closeSockets = false;
			WSACleanup();
		}
	}
	return 0;
}

/**
	Fonction qui fait fait la lecture du fichier contenant les candidats
	\param
	\return
	Note: Le fichier a un format sp�cifique : chaque ligne contient le nom
		  d'un candidat sans un s�parateur autre que la fin de ligne.

*/
void LireFichier()
{
	ifstream fichier;
	string nom = "";
	fichier.open("candidats.txt");

	if (fichier.is_open())
	{
		while (getline(fichier, nom))
		{
			resultat.insert(make_pair(nom, 0));
		}
		fichier.close();
	}
}

/**
	Fonction qui fait un affiche les r�sultats du vote
	\param 
	\return 
	Note: 

*/
void AfficherResultat()
{
	auto iter = resultat.begin();
	while (iter != resultat.end())
	{
		cout << endl << iter->first << " avec " << to_string(iter->second);
		iter++;
	}
	cout << "\n\n";
}